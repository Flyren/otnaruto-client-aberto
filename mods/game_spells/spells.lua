local spellsWindow
local spellsButton
local spellsList
local spellsInfo
local spellsButtons
local currentSpell
local language_id
local setSlotPanel

languageID = 
{
 ["en"] = 1,
 ["pt"] = 2,
}

function init()
  spellsWindow = g_ui.displayUI('spells')
  spellsWindow:hide()
  spellsList = spellsWindow:getChildById('spellsList')
  spellsButtons = spellsWindow:getChildById('spellsButtons')
  setSlotPanel = spellsWindow:getChildById('setSlotPanel')
  
  connect(g_game, { onLogout = hide } )
  connect(g_game, { onGameEnd = hide } )

  connect(spellsList, { onChildFocusChange = function(self, focusedChild)
                          if focusedChild == nil then return end
                              showSpell(focusedChild:getId())
                        end })
                        
  g_keyboard.bindKeyPress('Up', function() spellsList:focusPreviousChild(KeyboardFocusReason) end, spellsWindow)
  g_keyboard.bindKeyPress('Down', function() spellsList:focusNextChild(KeyboardFocusReason) end, spellsWindow)

  spellsButton = modules.client_topmenu.addRightGameToggleButton('spellsButton', tr('Jutsus Window'), '/images/topbuttons/jutsus', toggle)
  
  language_id = languageID[ g_settings.get('locale', 'false')]
    if not language_id then
       language_id = 1
    end
  
  ProtocolGame.registerExtendedOpcode(OPCODE_SPELL_LIST, onSpellList)
  updateSpells()
end

local spells = {}

function onSpellList(protocol, opcode, buffer)
  local data = loadstring("return " .. buffer)()
  spells = data
  updateSpells()
end

function updateSpells()
  spellsList:destroyChildren()

  for i,v in pairs(spells) do
    local spell = JUTSUS[v]
    if spell then
      local widget = g_ui.createWidget("SpellItem", spellsList)
      widget:setId("spell_" .. v)
      widget:setImageSource('/images/game/jutsus/' .. v)
      widget:setText(spell.name)
      widget:setVisible(true)
      local req = widget:recursiveGetChildById("reqLevel")
      if(req and spell.req) then
         req:setText("Lv: ".. spell.req)
      end
    end
  end
end

function split_string(string, sep)
  local sep, fields = sep or ":", {}
  local pattern = string.format("([^%s]+)", sep)
  string:gsub(pattern, function(c) fields[#fields+1] = c end)
  return fields
end

function showSpell(spell_id)
  local id = split_string(spell_id, "_")[2] * 1
  local info = JUTSUS[id]
  if not info then return false end

  currentSpell = id
  local img = spellsWindow:recursiveGetChildById("spellImage")
  img:setImageSource('/images/game/jutsus/' .. id)
  img:setBorderWidth(1)
  img:setBorderColor("#000000AA")

  spellsWindow:recursiveGetChildById("spellName"):setText(info.name)
  spellsWindow:recursiveGetChildById("spellReq"):setText("Required Level: ".. info.req)
  spellsWindow:recursiveGetChildById("spellDesc"):setText(info.desc[language_id])

  local text = ""                                                  

  setSlotPanel:setVisible(true)
  if (info.passive) then
    if (info.passive == 1) then
       text = text .. "\n ::: PASSIVE ::: \n"
       setSlotPanel:setVisible(false)
    end
  end

  if(info.damage) then
    text = text .. "Attack: " .. info.damage .. "\n"
  end

  if(info.casting) then
    text = text .. "Casting Time: " .. info.casting  .. "s\n"
  end

  if(info.duration) then
   text = text .. "Duration: " .. info.duration .. "s\n"
  end

  if info.costs then
    if info.costs.chakra or info.costs.hp then
      text = text .. "\nCosts:\n"
      if( info.costs.hp) then
        text = text .. "    HP: " ..  info.costs.hp  .. "\n"
      end

      if(info.costs.chakra) then
        text = text .. "    Chakra: " .. info.costs.chakra  .. "\n"
      end
    end
  end

  if (info.target) then
    if(info.target == 1) then
      text = text .. "\nNeed Target Position!\n"
    end
  end

 spellsWindow:recursiveGetChildById("spellOther"):setText(text)
 

end

function terminate()
  ProtocolGame.unregisterExtendedOpcode(OPCODE_SPELL_LIST)
  disconnect(g_game, { onLogout = hide } ) 
  disconnect(g_game, { onGameEnd = hide } )
  spellsWindow:destroy()
  spellsButton:destroy()
  spellsList = nil
end

function hide()
  spellsWindow:hide()
  spellsButton:setOn(false)
end

function show()
  spellsWindow:show()
  spellsButton:setOn(true)
  spellsWindow:raise()
  spellsWindow:focus()
end

function toggle()
  if spellsWindow:isVisible() then
    hide()
  else
    show()
  end
end

function setSpell(slot)
  if currentSpell then
    modules.game_spellbar.setSpellSlot(currentSpell *1, slot * 1)
  end
end