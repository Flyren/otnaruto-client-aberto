local spellBarPanel = nil
local spells = {0, 0, 0, 0, 0, 0, 0, 0}
local lastHotkeyTime = 0
local bottomPanel = nil

function init()
   spellBarPanel = g_ui.loadUI('spellbar', modules.game_interface.getHotkeyPanel())
   bottomPanel = modules.game_interface.getBottomPanel()
   
   if not g_game.isOnline() then 
     spellBarPanel:setVisible(false)  
   else
     spellBarPanel:setVisible(true) 
   end
   createIcons()

   connect(g_game, {
    onLogout = hide,
    onLogin = show,
    onGameEnd = hide,
    onGameStart = requestSpells
    })

   ProtocolGame.registerExtendedOpcode(OPCODE_SPELL_SLOTS, onReceiveSpells)

   refresh()
end

function terminate()
   disconnect(g_game, {
    onLogout = hide,
    onLogin = show,
    onGameEnd = hide,
    onGameStart = requestSpells
    })
   
   spellBarPanel:destroyChildren()
   spellBarPanel:destroy()

   ProtocolGame.unregisterExtendedOpcode(OPCODE_SPELL_SLOTS)
end

function refresh()
  local player = g_game.getLocalPlayer()
  if not player then return end
  destroyIcons()
  createIcons()
end

function split_string(string, sep)
  local sep, fields = sep or ":", {}
  local pattern = string.format("([^%s]+)", sep)
  string:gsub(pattern, function(c) fields[#fields+1] = c end)
  return fields
end

function createIcons()
	spellBarPanel:destroyChildren()

	for i = 1, #spells do
		local spellId = spells[i]
    local icon

		if spellId < 1 then
		  icon = g_ui.createWidget('EmptyIcon', spellBarPanel)
      icon:setTooltip("Empty")
			icon.onClick = function() clickEmpty(i) end
		else
  			local info = JUTSUS[spellId]
        
  			if info.target == 1 then
  				icon = g_ui.createWidget('SpellTargetIcon', spellBarPanel)
  				icon.onClick = function() clickTargetSpell(spellId) end
  			else
  				icon = g_ui.createWidget('SpellIcon', spellBarPanel)
				  icon.onClick = function() clickSpell(spellId) end
  			end
        icon:setIcon('/images/game/jutsus/' .. spellId)
        local tooltip = info.name
        icon:setTooltip(tooltip)
		end
    icon:setId('Icon_'..i)  
    icon:recursiveGetChildById("iconHotkey"):setText(i)
    g_keyboard.bindKeyPress(i, function() hotkeySpellUse(i) end)
    spellBarPanel:setVisible(false)
    spellBarPanel:setVisible(true)
	end
end

function clickEmpty(slot)
end

function clickSpell(id)
	sendUseSpell(id)
end

function clickTargetSpell(id)
  modules.game_interface.setSelectSpell(id)
end

function destroyIcons()
  for i = 1, 8 do
    local icon = icons['Icon'..i]
    if icon then
      icon.icon:destroy()
    end
  end               
  icons = {}
end

function toggle()

end

function hide()	
   spellBarPanel:setVisible(false)
end

function show()
   spellBarPanel:setVisible(true)
   createIcons()
end

function sendUseSpell(id, pos)
	local protocolGame = g_game.getProtocolGame()
	if protocolGame then
    if pos then
      protocolGame:sendExtendedOpcode(OPCODE_SPELLHUD, json.encode({i=id,p={x=pos.x,y=pos.y,z=pos.z}}))
		else
      protocolGame:sendExtendedOpcode(OPCODE_SPELLHUD, json.encode({i=id}))
    end
    return true
	end
end

function requestSpells()
  local protocolGame = g_game.getProtocolGame()
  if protocolGame then
      protocolGame:sendExtendedOpcode(OPCODE_REQ_SPELL_SLOTS, json.encode(0))
    return true
  end
end

--json = require "json"

function onReceiveSpells(protocol, opcode, buffer)
  local data = loadstring("return " .. buffer)()
  spells = data
  createIcons()
end

function setSpellSlot(id, slot)
  local protocolGame = g_game.getProtocolGame()
  if protocolGame then
    protocolGame:sendExtendedOpcode(OPCODE_SPELL_SLOTS, json.encode({i=id,s=slot}))
    return true
  end
end

function hotkeySpellUse(slot)
  if slot < 1 or slot > 8 then return false end
  if bottomPanel:isVisible() then return false end

  local widget = spellBarPanel:getChildById('Icon_' .. slot)
  if not widget then return false end

  if g_clock.millis() - lastHotkeyTime < 800 then
    return
  end

  lastHotkeyTime = g_clock.millis()
  widget.onClick()
end
