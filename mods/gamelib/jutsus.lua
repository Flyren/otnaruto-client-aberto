OPCODE_SPELLHUD = 52
OPCODE_SPELL_SLOTS = 53
OPCODE_SPELL_LIST = 54
OPCODE_TRACK = 55
OPCODE_AUTOLOOT = 56
OPCODE_REQ_SPELL_SLOTS = 57
OPCODE_NINJA_RANK = 58

JUTSUS = 
{
    [1] = {
        name="Henge",
        type="Ninjutsu",
        req = 1,
        target = 1, -- name, req, target e desc sao os unicos atributos que precisam ter em TODOS!!!
        costs = {chakra=15},
        duration = 30,
        desc={"Copy the target's appearance, but is unable to run or attack.", "Copia a apar�ncia do alvo, por�m, fica impossibilitado de correr ou atacar."},   
    }, 
    
    [2] = {
        name="Bunshin",
        type="Ninjutsu",
        req = 1,
        target = 0,
        costs = {chakra=15, hp= 0},
        duration = 3,
        desc={"Create an illusion of yourself.", "Cria uma ilus�o sua."},
    },

    [3] = {
        name="Sharingan Nv 1",
        type="Kekkei Genkai",
        req = 1,
		target = 0,
        costs = {chakra="3/10seg", hp= 0},
        duration = 0,
        desc={"The first level of the Uchiha's kekkei genkai. Gives a bonus on combat skills.", "Primeiro level do kekkei genkai dos Uchiha. D� um bonus em skills de combate"},
    },

    [4] = {
        name="Sharingan Nv 2",
        type="Kekkei Genkai",
        req = 1,
       	target = 0,
        costs = {chakra="5/9seg", hp= 0},
        duration = 0,
        desc={"The second level of the Uchiha's kekkei genkai. Gives a bonus on combat skills.", "Segundo level do kekkei genkai dos Uchiha. D� um bonus em skills de combate"},
    },

	[5] = {
		name = "Burning Desire",
		req = 1,
		passive = 1,
        target = 0,
        desc={"The user train Katon skills faster than usual.", "O usu�rio treina Katon mais r�pido que o normal."},
	},

	[6] = {
		name = "Sharingan Nv 3",
		req = 1,
        target = 0,
        costs = {chakra="1/8seg", hp= 0},
        duration = 0,
        desc={"The third level of the Uchiha's kekkei genkai. Gives a bonus on combat skills.", "Terceiro level do kekkei genkai dos Uchiha. D� um bonus em skills de combate"},
    },

	[7] = {
		name = "Mangekyou Sharingan",
		req = 1,
        target = 0,
        costs = {chakra="1/8seg", hp= "5/seg"},
        duration = 0,
        desc={"The ultimate level of the Uchiha's kekkei genkai. Gives a bonus on combat skills.", "Ultimo level do kekkei genkai dos Uchiha. D� um bonus em skills de combate"},
    },

	[8] = {
		name = "Kagemane",
		req = 1,
		damage = 0,
        target = 0,
        costs = {chakra=30}, 
        duration = 0,
        desc={"", "T�cnica passada entre gera��es dentro do cl� Nara.o usu�rio tem a sua sombra estendida, assim que a sombra do usu�rio entrar em contato com a outra sombra, os movimentos da v�tima se tornam exclusivos do usu�rio desta t�cnica"},
	},

	[9] = {
		name = "Nara Healing Medicine",
		req = 1,
		damage = 0,
        target = 0,
        costs = {chakra=50}, 
        duration = 0,
        desc={"", "O usu�rio utiliza uma das t�cnicas de medicina do cl� para se curar."},
	},

	[10] = {
		name = "Kibaku Fuda",
		req = 1,
		damage = 0,
        target = 0,
        costs = {chakra=200}, 
        duration = 0,
        desc={"", "Utilizando uma tarja em branco, o usu�rio cria uma tarja explosiva"},
	},

	[11] = {
		name = "Kemuridama",
		req = 1,
		damage = 0,
        target = 0,
        costs = {chakra=350}, 
        duration = 0,
        desc={"","Utilizando uma tarja em branco, o usu�rio cria uma bomba de fuma�a."},
	},

	[12] = {
		name = "Gedokuyako",
		req = 1,
		damage = 0,
        target = 0,
        costs = {chakra=150}, 
        duration = 0,
        desc={"", "O membro do cl� Nara pode se curar de venenos utilizando os conhecimentos medicinais do cl�."},
	},

	[13] = {
		name = "Kage Kubishibari",
		req = 1,
		damage = 25,
        target = 0,
        costs = {chakra=50}, 
        duration = 0,
        desc={"", "Caso tenha algum inimigo preso no Kagemane, o usu�rio utiliza sua sombra para enforcar o inimigo."},
	},

	[14] = {
		name = "Kage Nui",
		req = 1,
		damage = 30,
        target = 0,
        costs = {chakra="200/seg"}, 
        duration = 0,
        desc={"", "Caso tenha um alvo preso no Kagemane ou o alvo do usu�rio esteja a 1sqm de dist�ncia de alguma sombra, as sombras se transformam em espinhos e perfuram o alvo."},
	},

	[15] = {
		name = "Will of Fire",
		req = 1,
		damage = 0,
        target = 0,
        costs = {chakra=0}, 
        duration = 0,
        desc={"", "O primeiro elemento do cl� Sarutobi � exclusivamente Katon. Membros do cl� Sarutobi aumentam o skill de Katon mais r�pido que o normal (1.1) e seus jutsus elementais (Katon ou n�o) causam 15% a mais de dano que o normal."},
	},

	[16] = {
		name = "Protector Spirit",
		req = 1,
		damage = 0,
        target = 0,
        costs = {chakra=100}, 
        duration = 0,
        desc={"", "Faz com que todos os inimigos ao redor abandonem seus alvos e ataquem o usu�rio."},
	},

	[17] = {
		name = "Training Weights",
		req = 1,
		damage = 0,
        target = 0,
        costs = {chakra=0}, 
        duration = 0,
        desc={"", "Membros do clan Lee conseguem aprimorar seus skills mais r�pido conforme seu limite de peso est� chegando ao m�ximo."},
	},

	[18] = {
		name = "Konoha Senpuu",
		req = 1,
		damage = 20,
        target = 0,
        costs = {chakra=20}, 
        duration = 0,
        desc={"", "Causa dano e empurra o alvo. Caso o alvo se choque contra algo, o paralisa."},
	},

	[19] = {
		name = "Hachimon: Kaimon (1�)",
		req = 1,
		damage = 0,
        target = 0,
        costs = {chakra=0}, 
        duration = 0,
        desc={"", "Abre o primeiro port�o de chakra."},

	},
	[20] = {
		name = "Hachimon: Kyomon (2�)",
		req = 1,
		damage = 0,
        target = 0,
        costs = {chakra=0}, 
        duration = 0,
        desc={"", "Abre o segundo port�o de chakra."},
	},

	[21] = {
		name = "Konoha Dai Senpuu",
		req = 1,
		damage = 25,
        target = 0,
        costs = {chakra=40}, 
        duration = 0,
        desc={"", "Causa dano em todos ao redor e os empurra. Caso o alvo se choque contra algo, o paralisa."},
	},

	[22] = {
		name = "Youth Power",
		req = 1,
		damage = 0,
        target = 0,
        costs = {chakra=0}, 
        duration = 0,
        desc={"", "Ap�s morrer, o membro do clan Lee revive com 10% de sua HP m�xima. O efeito n�o pode ocorrer duas vezes seguidas."},
	},

	[23] = {
		name = "Hachimon: Seimon (3�)",
		req = 1,
		damage = 0,
        target = 0,
        costs = {chakra=0}, 
        duration = 0,
        desc={"", "Abre o terceiro port�o de chakra."},
	},

	[24] = {
		name = "Hachimon: Shoumon (4�)",
		req = 1,
		damage = 0,
        target = 0,
        costs = {chakra=0}, 
        duration = 0,
        desc={"", "Abre o quarto port�o de chakra."},
	},

	[25] = {
		name = "Omote Renge",
		req = 1,
		damage = 30,
        target = 0,
        costs = {chakra=50}, 
        duration = 0,
        desc={"", "Caso tenha um alvo no sqm � frente, o joga para o alto e cai com ele, causando dano no alvo e a inimigos adjacentes."},
	},

	[26] = {
		name = "Dog",
		req = 1,
		damage = 0,
        target = 0,
        costs = {chakra=0}, 
        duration = 0,
        desc={"", "O usu�rio ganha um cachorrinho. Seu cachorro cresce conforme ganha experi�ncia, tornando-se mais forte junto com seu dono."},
	},

	[27] = {
		name = "Tracking",
		req = 1,
		damage = 0,
        target = 0,
        costs = {chakra=0}, 
        duration = 0,
        desc={"", "O usu�rio ganha uma op��o nova na lista de amigos: Localizar (Track). Ao utilizar isso, consegue ter uma no��o da dire��o de onde est� o alvo."},
	},

	[28] = {
		name = "Shikyaku",
		req = 1,
		damage = 0,
        target = 0,
        costs = {chakra="2/seg"}, 
        duration = 0,
        desc={"", "Concentra chakra em certas partes do corpo, aumentando sua velocidade e for�a e assumindo uma postura mais animal."},
	},

	[29] = {
		name = "Jujin Bunshin",
		req = 1,
		damage = 0,
        target = 0,
        costs = {chakra=50}, 
        duration = 0,
        desc={"", "Agora seu cachorro se transforma em seu usu�rio, podendo tamb�m receber os efeitos do Shikyaku no jutsu."},
	},
	
	[30] = {
		name = "Tsuuga",
		req = 1,
		damage = 35,
        target = 0,
        costs = {chakra=150}, 
        duration = 0,
        desc={"", "Gira em alt�ssima velocidade, causando dano a inimigos na reta e atravessando-os."},
	},

	[31] = {
		name = "Deep Wound",
		req = 1,
		damage = 10,
        target = 0,
        costs = {chakra=0}, 
        duration = 0,
        desc={"", "Os ataques do usu�rio e de seu c�o possuem 5% de chance de causarem feridas profundas nos alvos, fazendo-os levarem dano e deixarem marca de sangue ao se moverem."},
	},

	[32] = {
		name = "Byakugan",
		req = 1,
		damage = 0,
        target = 0,
        costs = {chakra="3/10seg"}, 
        duration = 0,
        desc={"", "Permite ao usu�rio ver barras de chakra dos inimigos e d� ao usu�rio chance de esquivar de qualquer dano f�sico. No primeiro uso faz aparecer durante 5s um �cone para cada criatura em um raio de 50 pisos ao redor."},
	},

	[33] = {
		name = "Juuken",
		req = 1,
		damage = 0,
        target = 0,
        costs = {chakra=0}, 
        duration = 0,
        desc={"", "Ataques b�sicos com o Byakugan ativado removem chakra do inimigo. Caso o inimigo n�o tenha chakra, o valor � adicionado ao dano normal."},
	},

	[34] = {
		name = "Shoutei",
		req = 1,
		damage = 12,
        target = 0,
        costs = {chakra=15}, 
        duration = 0,
        desc={"", "Causa dano, empurra e caso o alvo se choque contra algo, o paralisa"},
	},

	[35] = {
		name = "Hakke Sanjuuni Shou (32)",
		req = 1,
		damage = 25,
        target = 0,
        costs = {chakra=50}, 
        duration = 0,
        desc={"", "Cria uma marca no ch�o e se prepara. Ap�s esse tempo, causa dano em todos os inimigos que estiverem dentro do c�rculo."},
	},

	[36] = {
		name = "Hakkeshou Kaiten",
		req = 1,
		damage = 12,
        target = 0,
        costs = {chakra=30}, 
        duration = 0,
        desc={"", "Gira por determinado tempo, empurrando e causando dano a inimigos ao redor e bloqueando dano pela dura��o da habilidade."},
	},

	[37] = {
		name = "Hakke Rokujuuyon Shou (64)",
		req = 1,
		damage = 25,
        target = 0,
        costs = {chakra=75}, 
        duration = 0,
        desc={"", "Agora, acerta cada inimigo 2x, causando o dobro do dano. Al�m disso, a �rea fica maior."},
	},

	[38] = {
		name = "Shugohakke Rokujuuyon Shou",
		req = 1,
		damage = 20,
        target = 0,
        costs = {chakra=0}, 
        duration = 0,
        desc={"", "Enquanto permanecer im�vel, cria uma �rea em c�rculo ao redor do usu�rio, protegendo ele qualquer criatura dentro. Caso algu�m de fora tente entrar no c�rculo, leva dano. Qualquer proj�til lan�ado � defletido"},
	},

	[39] = {
		name = "Hakke Kushou",
		req = 1,
		damage = 25,
        target = 0,
        costs = {chakra=65}, 
        duration = 0,
        desc={"", "Vers�o mais forte do Shoutei, podendo ser lan�ado � dist�ncia, causando dano ao primeiro inimigo encontrado, empurrando-o 1sqm e caso o alvo se choque contra algo, o paralisando-o"},
	},

	[40] = {
		name = "Food Lover",
		req = 1,
		damage = 0,
        target = 0,
        costs = {chakra=0}, 
        duration = 0,
        desc={"", "As comidas tem 50% menos dura��o nos membros do clan Akimichi, contudo, sua regenera��o de HP/Chakra � acelerada."},
	},

	[41] = {
		name = "Baika no Jutsu",
		req = 1,
		damage = 0,
        target = 0,
        costs = {chakra="2/seg"}, 
        duration = 0,
        desc={"", "Reduz o dano recebido em 20%. Dura at� o usu�rio se mover por vontade pr�pria."},
	},

	[42] = {
		name = "Nikudan Sensha",
		req = 1,
		damage = 20,
        target = 0,
        costs = {chakra=50}, 
        duration = 0,
        desc={"", "Se transforma em uma bola e rotaciona em alta velocidade em linha reta at� colidir com alguma barreira ou ap�s andar 7 sqms."},
	},

	[43] = {
		name = "Bubun Baika",
		req = 1,
		damage = 30,
        target = 0,
        costs = {chakra=60}, 
        duration = 0,
        desc={"", "Aumenta o tamanho de sua m�o e d� um tapa na �rea � frente, causando dano a todos os inimigos atingidos e reduzindo suas velocidades por 1s."},
	},

	[44] = {
		name = "Nikudan Hari Sensha",
		req = 1,
		damage = 30,
        target = 0,
        costs = {chakra=150}, 
        duration = 0,
        desc={"", "Vers�o aprimorada do Nikudan Sensha. Causa mais dano."},
	},

	[45] = {
		name = "Kikaichuu",
		req = 1,
		damage = 0,
        target = 0,
        costs = {chakra=60}, 
        duration = 0,
        desc={"", "Cria um aglomerado de kikkaichu com 10% da HP m�xima do usu�rio. Os ataques dos insetos drenam chakra."},
	},

	[46] = {
		name = "Mushi Bunshin",
		req = 1,
		damage = 0,
        target = 1,
        costs = {chakra=50}, 
        duration = 0,
        desc={"", "Transforma um aglomerado de insetos em um clone do usu�rio."},
	},

	[47] = {
		name = "Kidaichu",
		req = 1,
		damage = 0,
        target = 0,
        costs = {chakra=80}, 
        duration = 0,
        desc={"", "Cria um Kidaichu com 10% da HP m�xima do usu�rio. Os ataques dos insetos drenam chakra. Ap�s causar 200 de dano o inseto evolui para Nv 2."},
	},

	[48] = {
		name = "Kikkaichu Shot",
		req = 1,
		damage = 10,
        target = 0,
        costs = {chakra=0}, 
        duration = 0,
        desc={"", "Ataques b�sicos desarmado agora possuem +7 de ataque, drenam chakra e possuem 4 de alcance."},
	},

	[49] = {
		name = "Human Coccon",
		req = 1,
		damage = 0,
        target = 0,
        costs = {chakra=100}, 
        duration = 0,
        desc={"", "Caso n�o esteja com battle, se camufla em uma �rvore at� sofrer dano ou se mover."},
	},

	[50] = {
		name = "Mushikame",
		req = 1,
		damage = 35,
        target = 0,
        costs = {chakra=150}, 
        duration = 0,
        desc={"", "Cria uma barreira que bloqueia dano por um breve per�odo de tempo (2s)."},
	},

	[51] = {
		name = "Kikkaichu Yajiri",
		req = 1,
		damage = 35,
        target = 1,
        costs = {chakra="300/seg"}, 
        duration = 0,
        desc={"", "Espalha insetos numa �rea do ch�o. Os insetos ficam l� por 30s."},
	},

	[52] = {
		name = "Mushi Tatsumaki",
		req = 1,
		damage = 30,
        target = 1,
        costs = {chakra=500}, 
        duration = 0,
        desc={"", "Cria um tornado de insetos na �rea desejada, causando dano, drenando metade do dano como chakra e puxando as criaturas para o centro. Consome todos os insetos ativos. Para cada inseto ativo, aumenta o dano em 20%."},
	},
	[53] = {
        name="Sprint",
        type="Ninjutsu",
        req = 1,
        target = 0,
        costs = {chakra=15, hp= 0},
        duration = 20,
        desc={"Convert chakra in speed"},
    },
    [54] = {
        name="Healing",
        type="Ninjutsu",
        req = 1,
        target = 0,
        costs = {chakra=20, hp= 0},
        duration = 0,
        desc={"Convert chakra in health"},
    },
	[55] = {
        name="Katon: Housenka",
        type="Ninjutsu",
        req = 10,
        target = 0,
        costs = {chakra=20, hp= 0},
        duration = 0,
        desc={"Convert chakra in katon"},
    },
    [56] = {
        name="Katon: Endan",
        type="Ninjutsu",
        req = 15,
        target = 0,
        costs = {chakra=20, hp= 0},
        duration = 0,
        desc={"Convert chakra in katon"},
    },
    [57] = {
        name="Shokaichu",
        type="Ninjutsu",
        req = 10,
        target = 0,
        costs = {chakra=20, hp= 0},
        duration = 0,
        desc={"Convert chakra in insects"},
    },
    [58] = {
        name="Kage Wave",
        type="Ninjutsu",
        req = 10,
        target = 0,
        costs = {chakra=20, hp= 0},
        duration = 0,
        desc={"Convert chakra in shadow"},
    },
    [59] = {
        name="Create shuriken",
        type="Ninjutsu",
        req = 5,
        target = 0,
        costs = {chakra=40, hp= 0},
        duration = 0,
        desc={"Convert chakra in weapons"},
    },
    [60] = {
        name="Create Kunai",
        type="Ninjutsu",
        req = 5,
        target = 0,
        costs = {chakra=40, hp= 0},
        duration = 0,
        desc={"Convert chakra in weapons"},
    },
    [61] = {
        name="Insect Throw",
        type="Ninjutsu",
        req = 20,
        target = 1,
        costs = {chakra=20, hp= 0},
        duration = 0,
        desc={"Convert chakra in insects"},
    },
}