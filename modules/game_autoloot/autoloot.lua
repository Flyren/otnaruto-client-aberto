local _parseOpcode
local _parseItemsList
local _parseCollectedItems

local autoLootWindow
local colledtedItemsPanel
local topMenuButton
local itemsPanelList

local autoLootOpcode = 56

function init()
  autoLootWindow = g_ui.loadUI('autoloot', modules.game_interface.getRightPanel())

  topMenuButton = modules.client_topmenu.addRightGameToggleButton('topMenuButton', tr('AutoLoot Information'), '/images/topbuttons/autoloot', toggle)
  
  colledtedItemsPanel = g_ui.createWidget('AutoLootCollectedPanel', rootWidget)
  itemsPanelList = autoLootWindow:recursiveGetChildById('itemsPanelList')

  g_game.handleExtendedOpcode(autoLootOpcode, _parseOpcode)

  if g_game.isOnline() then
    _parseItemsList(G.parsedAutolootItems or {})
  end

  connect(g_game, { onGameEnd = offline })

  autoLootWindow:setup()
end

function terminate()
  autoLootWindow:destroy()
  topMenuButton:destroy()
  colledtedItemsPanel:destroy()

  g_game.unhandleExtendedOpcode(autoLootOpcode)

  disconnect(g_game, { onGameEnd = offline })
end

function offline()
  colledtedItemsPanel:destroyChildren()
end

function toggle()
  if autoLootWindow:isVisible() then
    autoLootWindow:close()
    topMenuButton:setOn(false)
  else
    autoLootWindow:open()
    topMenuButton:setOn(true)
  end
end

function onMiniWindowClose()
  topMenuButton:setOn(false)
end

function _parseOpcode(action, data)
  if action == 'updateList' then
    _parseItemsList(data.items)

  elseif action == 'collectedItems' then
    _parseCollectedItems(data.items)

  end
end

function _parseItemsList(items)
  itemsPanelList:destroyChildren()

  G.parsedAutolootItems = items
  
  for _, item in ipairs(items) do
    local widget = g_ui.createWidget('AutoLootItem', itemsPanelList)
    local itemWidget = widget:getChildById('previewItem')
    itemWidget:setItemId(item.spriteId)

    local name = widget:getChildById('nameItem')
    name:setText(item.name)
  end
end

function _parseCollectedItems(items)
  for _, item in ipairs(items) do
    local widget = g_ui.createWidget('CollectedItem', colledtedItemsPanel)
    widget:setItemId(item.spriteId)
    widget:getChildById('count'):setText(item.count)

    scheduleEvent(function()
      g_effects.fadeOut(widget, 500)
      scheduleEvent(function()
        widget:destroy()
      end, 600)
    end, 2000)
  end
end

function requestUpdate()
  g_game.sendOpcode(autoLootOpcode, "requestUpdate")
end

function addAutoLoot(thing)
  g_game.sendOpcode(autoLootOpcode, "addItem", {
    spriteId = thing:getId(),
  })
end

function removeAutoLoot(spriteId)
  g_game.sendOpcode(autoLootOpcode, "removeItem", { 
    spriteId = spriteId 
  })
end

function clearAll()
  g_game.sendOpcode(autoLootOpcode, "clearAll")
end
