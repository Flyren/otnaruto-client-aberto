InventorySlotStyles = {
  [InventorySlotHead] = "HeadSlot",
  [InventorySlotNeck] = "NeckSlot",
  [InventorySlotBack] = "BackSlot",
  [InventorySlotBody] = "BodySlot",
  [InventorySlotRight] = "RightSlot",
  [InventorySlotLeft] = "LeftSlot",
  [InventorySlotLeg] = "LegSlot",
  [InventorySlotFeet] = "FeetSlot",
  [InventorySlotFinger] = "FingerSlot",
  [InventorySlotAmmo] = "AmmoSlot"
}

Backgrounds = 
{
	[0] = { path = '/images/inventory/anbu.png', id = 'background_anbu'},
	[1] = { path = '/images/inventory/uchiha.png', id = 'background_uchiha'},
	[2] = { path = '/images/inventory/hyuuga.png', id = 'background_hyuuga'},
	[3] = { path = '/images/inventory/aburame.png', id = 'background_aburame'},
	[4] = { path = '/images/inventory/akimichi.png', id = 'background_akimichi'},
	[5] = { path = '/images/inventory/inuzuka.png', id = 'background_inuzuka'},
	[6] = { path = '/images/inventory/nara.png', id = 'background_nara'},
	[7] = { path = '/images/inventory/maito.png', id = 'background_maito'},
	[8] = { path = '/images/inventory/anbu.png', id = 'background_anbu'},
}

inventoryWindow = nil
inventoryPanel = nil
inventoryButton = nil

function init()
  connect(LocalPlayer, { onInventoryChange = onInventoryChange })
  connect(g_game, { onGameStart = refresh })
  connect(g_game, { onGameEnd = offline })

  g_keyboard.bindKeyDown('Ctrl+I', toggle)

  inventoryButton = modules.client_topmenu.addRightGameToggleButton('inventoryButton', tr('Inventory') .. ' (Ctrl+I)', '/images/topbuttons/inventory', toggle)
  inventoryButton:setOn(true)

  inventoryWindow = g_ui.loadUI('inventory', modules.game_interface.getRightPanel())
  inventoryWindow:disableResize()
  inventoryPanel = inventoryWindow:getChildById('contentsPanel')

  local itemWidget = inventoryPanel:getChildById('slot9')
  itemWidget:setVisible(false)
 -- local itemWidget = inventoryPanel:getChildById('slot10')
  --itemWidget:setVisible(false)
           
  refresh()
  inventoryWindow:setup()
  refresh()
  inventoryWindow:close()
    inventoryButton:setOn(false)
    
end

function terminate()
  disconnect(LocalPlayer, { onInventoryChange = onInventoryChange })
  disconnect(g_game, { onGameStart = refresh })

  g_keyboard.unbindKeyDown('Ctrl+I')

  inventoryWindow:destroy()
  inventoryButton:destroy()
end

function offline()
  inventoryButton:setOn(false)
  inventoryWindow:close()
end

function refresh()
  local player = g_game.getLocalPlayer()   
  for i=InventorySlotFirst,InventorySlotLast do
    if g_game.isOnline() then
      onInventoryChange(player, i, player:getInventoryItem(i))
      local vocation = player:getVocation()
      if(vocation ~= 0) then
      	inventoryPanel:setImageSource(Backgrounds[vocation].path)
      end
    else
      onInventoryChange(player, i, nil)
    end
  end

end   

function toggle()
  if inventoryButton:isOn() then
    inventoryWindow:close()
    inventoryButton:setOn(false)
  else
    inventoryWindow:open()
    inventoryButton:setOn(true)
  end
  refresh()
end

function onMiniWindowClose()
  inventoryButton:setOn(false)
end

-- hooked events
function onInventoryChange(player, slot, item, oldItem)
     --[[ local player = g_game.getLocalPlayer()
      if(player) then 
	      local vocation = player:getVocation()
	      print("1vocation: ".. vocation)
	      if(vocation ~= 0) then
	      	inventoryWindow:setImageSource(Backgrounds[vocation].path)
	      end
      end]]
      
  if slot >= InventorySlotPurse then return end
  local itemWidget = inventoryPanel:getChildById('slot' .. slot)
  if not itemWidget then return end
  if item then
    itemWidget:setStyle('Item')
    itemWidget:setItem(item)
  else
    itemWidget:setStyle(InventorySlotStyles[slot])
    itemWidget:setItem(nil)
  end
end